const peopleContent = document.querySelector('.people-content')
const planetsContent = document.querySelector('.planets-content')
const vehiclesContent = document.querySelector('.vehicles-content')

let nextPeople = null;
let nextPlanets = null;
let nextVehicles = null;

const showDetails = (details, flag) => {
    const detailsContent = document.querySelector('.details-content')
    switch (flag) {
        case 'people': {
            return detailsContent.innerHTML =
                `<div>
                        <div>name: ${details.name}</div>
                        <div>mass: ${details.mass}</div>
                        <div>skin color: ${details.skin_color}</div>
                        <div>height: ${details.height}</div>
                        <div>hair color: ${details.hair_color}</div>
                        <div>gender:${details.gender}</div>
                        <div>eye color:${details.eye_color}</div>
                    </div>`
            break;
        }
        case 'planets': {
            return detailsContent.innerHTML =
                `<div>
                        <div>name: ${details.name}</div>
                        <div>climate: ${details.climate}</div>
                        <div>diameter: ${details.diameter}</div>
                        <div>gravity: ${details.gravity}</div>
                        <div>orbital period: ${details.orbital_period}</div>
                        <div>surface water:${details.surface_water}</div>
                        <div>population:${details.population}</div>
                    </div>`
            break;
        }

        case 'vehicles': {
            return detailsContent.innerHTML =
                `<div>
                        <div>name: ${details.name}</div>
                        <div>crew: ${details.crew}</div>
                        <div>length: ${details.length}</div>
                        <div>model: ${details.model}</div>
                        <div>passengers: ${details.passengers}</div>
                        <div>cost_in_credits:${details.cost_in_credits}</div>
                        <div>cargo_capacity:${details.cargo_capacity}</div>
                    </div>`
            break;
        }
        default: {
            detailsContent.innerHTML = 'No Data'
        }
    }
}

function getServerData(url, container, nextUrl, indicator, nextBtnDOMEl) {
    fetch(url)
        .then(res => res.json())
        .then((data) => {
            container.innerHTML = ''
            nextUrl = data.next;

            const items = data.results

            for (let i = 0; i < items.length; i++) {
                const listItem = document.createElement('div');
                listItem.classList.add('orange')
                listItem.textContent = items[i].name;
                container.appendChild(listItem);

                listItem.addEventListener('click', function () {
                    showDetails(items[i], indicator)
                })
            }

            const nextButton = document.querySelector(nextBtnDOMEl);
            nextButton.style.display = 'block'
            nextButton.addEventListener('click', function () {

                if (nextUrl === null) {
                    nextButton.style.display = 'none'
                }
                fetch(nextUrl)
                    .then(res => res.json())
                    .then(
                        (data) => {
                            nextUrl = data.next
                            container.innerHTML = ''
                            const items = data.results
                            for (let i = 0; i < items.length; i++) {
                                const listItem = document.createElement('div');
                                listItem.classList.add('orange')
                                listItem.textContent = items[i].name;
                                container.appendChild(listItem);

                                listItem.addEventListener('click', function () {
                                    showDetails(items[i], indicator)
                                })
                            }
                        }
                    )
            })
        });
}

document.getElementById('people').addEventListener('click', () => {
    getServerData(
        'https://swapi.dev/api/people',
        peopleContent,
        nextPeople,
        'people',
        '.next_people'
    )
})

document.getElementById('planets').addEventListener('click', () => {
    getServerData(
        'https://swapi.dev/api/planets',
        planetsContent,
        nextPlanets,
        'planets',
        '.next_planets'
    )
})

document.getElementById('vehicles').addEventListener('click', () => {
    getServerData(
        'https://swapi.dev/api/vehicles',
        vehiclesContent,
        nextVehicles,
        'vehicles',
        '.next_vehicles'
    )
});

